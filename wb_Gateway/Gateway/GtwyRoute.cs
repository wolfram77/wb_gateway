﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Wasp.Net
{
	public struct GtwyConn
	{
		public NetConn Ext;
		public NetConn Int;
		public NetConn Link;

		public GtwyConn(NetConn extConn = default(NetConn), NetConn intConn = default(NetConn), NetConn linkConn = default(NetConn))
		{
			Ext = extConn;
			Int = intConn;
			Link = linkConn;
		}
	}
	public class GtwyRoute
	{
		ConcurrentDictionary<NetConn, GtwyConn> connExt;
		ConcurrentDictionary<NetConn, GtwyConn> connLink;
		public int Conns
		{
			get { return connLink.Count; }
		}

		public GtwyRoute()
		{
			connExt = new ConcurrentDictionary<NetConn, GtwyConn>();
			connLink = new ConcurrentDictionary<NetConn, GtwyConn>();
		}

		public GtwyConn FromExt(NetConn ext)
		{
			GtwyConn conn = new GtwyConn(ext);
			return connExt.GetOrAdd(ext, conn);
		}
		public GtwyConn FromLink(NetConn link, NetConn intConn)
		{
			GtwyConn conn = new GtwyConn(linkConn: link);
		}
		public GtwyConn Add(GtwyConn conn)
		{
			connExt.AddOrUpdate(conn.Ext, conn, null);
			connLink.AddOrUpdate(conn.Link, conn, null);
			return conn;
		}
		public void Remove(NetConn link)
		{
			GtwyConn gtwyConn;
			connExt.TryRemove(link, out gtwyConn);
			connLink.TryRemove(gtwyConn.Link, out gtwyConn);
		}
	}
}
