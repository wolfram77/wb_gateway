﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace Wasp.Net
{
	public class GtwyHost
	{
		GtwyRoute route;
		NetHost extHost, intHost;
		NetEventFnGroup extEventFn, intEventFn;
		public GtwyRoute Route
		{
			get { return route; }
		}
		public NetHost ExtHost
		{
			get { return extHost; }
		}
		public NetHost IntHost
		{
			get { return intHost; }
		}
		public int Conns
		{
			get
			{ return route.Conns; }
		}

		public GtwyHost(IPEndPoint extAddr, IPEndPoint intAddr)
		{
			// init routing table
			route = new GtwyRoute();
			// setup event handlers
			extEventFn = new NetEventFnGroup(ext_OnRx, ext_OnTx, ext_OnAdd, ext_OnLink, ext_OnBreak);
			intEventFn = new NetEventFnGroup(int_OnRx, int_OnTx, int_OnAdd, int_OnLink, int_OnBreak);
			// startup external & internal hosts
			extHost = new NetHost(extAddr, extEventFn);
			intHost = new NetHost(intAddr, intEventFn, true);
		}

		void ext_OnRx(object sender, NetEventArgs args)
		{
			// receive packet
			GtwyConn conn;
			NetPkt rxPkt = extHost.Rx();
			// get routing info
			bool got = route.ConnExt.TryGetValue(rxPkt.Conn, out conn);
			try
			{
				connId = route.ConnLink[rxPkt.Conn];
				conn = route.ConnExt[connId];
			}
			catch ( Exception )
			{ connId = route.Add(new GtwyConn(rxPkt.Conn)); }
			// build gateway packet, send
			GtwyReq req = (conn.Int.Remote != null) ? GtwyReq.Rx : GtwyReq.Link;
			GtwyPkt txPkt = new GtwyPkt(req, GtwyStatus.Req, connId, conn.Int.Remote, rxPkt.Conn.Remote, rxPkt.Data);
			intHost.Tx(conn.Int, new DataBlk(txPkt.Write()));
		}
		void ext_OnTx(object sender, NetEventArgs args)
		{
			// get sent packet
			NetPkt rxPkt = (NetPkt) args.Data;
			// get route to send back ack / nack
			int connId = route.ConnLink[rxPkt.Conn];
			NetConn intAddr = route.ConnExt[connId].Int;
			// build gateway packet
			GtwyStatus status = (args.Error == null) ? GtwyStatus.Ack : GtwyStatus.Nack;
			GtwyPkt txPkt = new GtwyPkt(GtwyReq.Tx, status, connId);
			// send the ack/nack to the sender
			intHost[intAddr].Tx(new DataBlk(txPkt.Write()));
		}
		void ext_OnAdd(object sender, NetEventArgs args)
		{
			// get new client
			NetClient clnt = (NetClient) sender;
			int connId = route.Add(new GtwyConn(clnt.Conn));
			// send link request to all internal clients
			GtwyPkt txPkt = new GtwyPkt(GtwyReq.Link, GtwyStatus.Req, connId, null, clnt.Conn);
			intHost.Tx(new DataBlk(txPkt.Write()));
		}
		void ext_OnLink(object sender, NetEventArgs args)
		{
			// get connection client
			NetClient clnt = (NetClient) sender;
			int connId = (int) clnt.Info;
			GtwyConn conn = route.ConnExt[connId];
			// send connection status to connector
			GtwyStatus status = (args.Error == null) ? GtwyStatus.Ack : GtwyStatus.Nack;
			GtwyPkt txPkt = new GtwyPkt(GtwyReq.Link, status, connId, conn.Link, conn.Ext);
			intHost[conn.Int].Tx(new DataBlk(txPkt.Write()));
		}
		void ext_OnBreak(object sender, NetEventArgs args)
		{
			// get connection info
			int connId = 0;
			NetClient clnt = (NetClient) sender;
			IPEndPoint intAddr = null;
			try
			{
				connId = route.ConnLink[clnt.Conn];
				intAddr = route.ConnExt[connId].Int;
			}
			catch ( Exception ) { }
			// report disconnect to internal client
			GtwyPkt txPkt = new GtwyPkt(GtwyReq.Break, GtwyStatus.Ack, connId);
			intHost.Tx(intAddr, new DataBlk(txPkt.Write()));
			route.Remove(connId);
		}

		void intRx_OnRx(IPEndPoint addr, GtwyPkt rxPkt)
		{
		}
		void intRx_OnTx(IPEndPoint addr, GtwyPkt rxPkt)
		{
			IPEndPoint extAddr = route.ConnExt[rxPkt.ConnId].Ext;
			extHost[extAddr].Tx(rxPkt.Data);
		}
		void intRx_OnLink(IPEndPoint addr, GtwyPkt rxPkt)
		{

		}
		void intRx_OnBreak(IPEndPoint addr, GtwyPkt rxPkt)
		{
			// disconnect public client, remove from routing table
			IPEndPoint pubClnt = route.ConnExt[rxPkt.ConnId].Ext;
			extHost[pubClnt].Break();
			route.Remove(rdPkt.ConnId);
			((List<int>) intHost[rxPkt.Addr].Info).Remove(rdPkt.ConnId);
			// report disconnect ok status back to local client
			rdPkt = new GtwyPkt(GtwyReq.Break, GtwyStatus.Ack, rdPkt.ConnId);
			intHost[rxPkt.Addr].Tx(new DataBlk(rdPkt.GetBytes()));
		}
		
		void int_OnRx(object sender, NetEventArgs args)
		{
			// receive packet
			NetPkt rxPkt = intHost.Rx();
			GtwyPkt rdPkt = new GtwyPkt(rxPkt.Data.Src, rxPkt.Data.Off);
			if ( rdPkt.Status != GtwyStatus.Req ) return;
			try
			{
				switch ( rdPkt.Req )
				{
					case GtwyReq.Link:
						// if already accepted, then error
						if ( route.ConnExt[rdPkt.ConnId].Int != null ) throw new FieldAccessException();
						// update routing table as per request
						route.ConnExt[rdPkt.ConnId].Int = rxPkt.Addr;
						route.ConnExt[rdPkt.ConnId].Link = rdPkt.Src;
						((List<int>) intHost[rxPkt.Addr].Info).Add(rdPkt.ConnId);
						rdPkt = new GtwyPkt(GtwyReq.Link, GtwyStatus.Ack, rdPkt.ConnId, rdPkt.Dest, rdPkt.Src);
						intHost[rxPkt.Addr].Tx(new DataBlk(rdPkt.Write()));
						// break;
						// case GtwyReq.Link:
						// add to routing table, connect
						int connId = route.Add(new GtwyConn(rdPkt.Dest, rxPkt.Addr, rdPkt.Src));
						((List<int>) intHost[rxPkt.Addr].Info).Add(connId);
						NetClient publicClient = new NetClient(null, null, null);
						extHost.Add(publicClient);
						publicClient.Info = connId;
						publicClient.Link(rdPkt.Dest);
						break;
					case GtwyReq.Break:
						break;
				}
			}
			catch ( Exception )
			{
				rdPkt = new GtwyPkt(rdPkt.Req, GtwyStatus.Nack, rdPkt.ConnId, rdPkt.Dest, rdPkt.Src);
				intHost[rxPkt.Addr].Tx(new DataBlk(rdPkt.Write()));
			}
		}
		void int_OnTx(object sender, NetEventArgs args)
		{
			if ( args.Error == null )
				Interlocked.Add(ref intTxPkts, 1);
		}
		void int_OnAdd(object sender, NetEventArgs args)
		{
			// init connection list
			NetClient client = (NetClient) sender;
			client.Info = new List<int>();
		}
		void int_OnLink(object sender, NetEventArgs args)
		{
			int_OnAdd(sender, args);
		}
		void int_OnBreak(object sender, NetEventArgs args)
		{
			// get connection list of the internal client
			NetClient clnt = (NetClient) sender;
			List<int> connIds = (List<int>) clnt.Info;
			// terminate each connection
			foreach ( int connId in connIds )
			{
				try
				{
					IPEndPoint extAddr = route.ConnExt[connId].Ext;
					extHost[extAddr].Break();
					route.Remove(connId);
				}
				catch ( Exception ) { }
			}
			// clear connection list
			connIds.Clear();
			clnt.Info = null;
		}
		public new string ToString()
		{
			if ( extHost == null || intHost == null ) return "<>";
			return string.Format("<Ext- {0}, Int- {1}>", extHost.ToString(), intHost.ToString());
		}
		public void Close()
		{
			extHost.Close();
			intHost.Close();
		}
	}
}
