﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Wasp.Type;

namespace Wasp.Net
{
	public enum GtwyReq
	{
		Rx,
		Tx,
		Link,
		Break,
	}
	public enum GtwyStatus
	{
		Req,
		Ack,
		Nack
	}
	public struct GtwyPkt
	{
		/*
		 * gateway protocol
		 * ----------------
		 * 
		 * gateway runs upon TCP.
		 * [size: 2]	size of packet, including this field
		 * [request: 1]	request to be performed or status about
		 * [status: 1]	additional status information about request
		 * [dest: 6]	destination ip endpoint
		 * [src: 6]		source ip endpoint
		 * ... (data)
		 * 
		 */
		public int Size
		{
			get
			{ return HdrSize + Data.Size; }
		}
		public GtwyReq Req;
		public GtwyStatus Status;
		public NetConn Conn;
		public DataBlk Data;
		public const int HdrSize = 16;

		public GtwyPkt(byte[] src, int off)
		{
			// load up all the fields
			int size = BitConverter.ToUInt16(src, off);
			size = Math.Min(src.Length - off, size);
			off += 2;
			Req = (GtwyReq) src[off];
			off += 1;
			Status = (GtwyStatus) src[off];
			off += 1;
			Conn = new NetConn(src, off);
			off += 12;
			// packet data
			Data = new DataBlk(src, off, size - HdrSize);
		}
		public GtwyPkt(GtwyReq req, GtwyStatus status, NetConn conn = default(NetConn), DataBlk data = default(DataBlk))
		{
			Req = req;
			Status = status;
			Conn = conn;
			Data = data;
		}

		public byte[] GetBytes()
		{
			byte[] dst = new byte[Size];
			Write(dst, 0);
			return dst;
		}
		public void Write(byte[] dst, int off)
		{
			// store the standard fields
			Array.Copy(BitConverter.GetBytes(Size), 0, dst, off, 2);
			off += 2;
			Array.Copy(BitConverter.GetBytes((int) Req), 0, dst, off, 1);
			off += 1;
			Array.Copy(BitConverter.GetBytes((int) Status), 0, dst, off, 1);
			off += 1;
			Conn.Write(dst, off);
			off += 12;
			// copy packet data
			if ( Data.Size > 0 ) Array.Copy(Data.Src, Data.Off, dst, off, Data.Size);
		}
	}
}
