﻿namespace wb_Gateway
{
	partial class MainWin
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if ( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWin));
			this.PublicLabel = new System.Windows.Forms.Label();
			this.PublicIpComboBox = new System.Windows.Forms.ComboBox();
			this.PublicPortLabel = new System.Windows.Forms.Label();
			this.PublicIpLabel = new System.Windows.Forms.Label();
			this.PublicPortTextBox = new System.Windows.Forms.TextBox();
			this.PrivateLabel = new System.Windows.Forms.Label();
			this.PrivatePortTextBox = new System.Windows.Forms.TextBox();
			this.PrivateIpLabel = new System.Windows.Forms.Label();
			this.PrivatePortLabel = new System.Windows.Forms.Label();
			this.PrivateIpComboBox = new System.Windows.Forms.ComboBox();
			this.PublicClientsLabel = new System.Windows.Forms.Label();
			this.PublicSentLabel = new System.Windows.Forms.Label();
			this.PublicReceivedLabel = new System.Windows.Forms.Label();
			this.PrivateReceivedLabel = new System.Windows.Forms.Label();
			this.PrivateSentLabel = new System.Windows.Forms.Label();
			this.PrivateClientsLabel = new System.Windows.Forms.Label();
			this.LinksLabel = new System.Windows.Forms.Label();
			this.LinksPictureBox = new System.Windows.Forms.PictureBox();
			this.PrivateReceivedPictureBox = new System.Windows.Forms.PictureBox();
			this.PrivateSentPictureBox = new System.Windows.Forms.PictureBox();
			this.PrivateClientsPictureBox = new System.Windows.Forms.PictureBox();
			this.PublicReceivedPictureBox = new System.Windows.Forms.PictureBox();
			this.PublicSentPictureBox = new System.Windows.Forms.PictureBox();
			this.PublicClientsPictureBox = new System.Windows.Forms.PictureBox();
			this.PublicPictureBox = new System.Windows.Forms.PictureBox();
			this.PrivatePictureBox = new System.Windows.Forms.PictureBox();
			this.StartButton = new System.Windows.Forms.Button();
			this.WinTimer = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.LinksPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PrivateReceivedPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PrivateSentPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PrivateClientsPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PublicReceivedPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PublicSentPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PublicClientsPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PublicPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PrivatePictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// PublicLabel
			// 
			this.PublicLabel.AutoSize = true;
			this.PublicLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PublicLabel.Location = new System.Drawing.Point(161, 143);
			this.PublicLabel.Name = "PublicLabel";
			this.PublicLabel.Size = new System.Drawing.Size(76, 32);
			this.PublicLabel.TabIndex = 0;
			this.PublicLabel.Text = "Public";
			// 
			// PublicIpComboBox
			// 
			this.PublicIpComboBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PublicIpComboBox.FormattingEnabled = true;
			this.PublicIpComboBox.Location = new System.Drawing.Point(83, 188);
			this.PublicIpComboBox.Name = "PublicIpComboBox";
			this.PublicIpComboBox.Size = new System.Drawing.Size(156, 33);
			this.PublicIpComboBox.TabIndex = 1;
			// 
			// PublicPortLabel
			// 
			this.PublicPortLabel.AutoSize = true;
			this.PublicPortLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PublicPortLabel.Location = new System.Drawing.Point(245, 191);
			this.PublicPortLabel.Name = "PublicPortLabel";
			this.PublicPortLabel.Size = new System.Drawing.Size(49, 25);
			this.PublicPortLabel.TabIndex = 2;
			this.PublicPortLabel.Text = "Port:";
			// 
			// PublicIpLabel
			// 
			this.PublicIpLabel.AutoSize = true;
			this.PublicIpLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PublicIpLabel.Location = new System.Drawing.Point(46, 191);
			this.PublicIpLabel.Name = "PublicIpLabel";
			this.PublicIpLabel.Size = new System.Drawing.Size(31, 25);
			this.PublicIpLabel.TabIndex = 4;
			this.PublicIpLabel.Text = "IP:";
			// 
			// PublicPortTextBox
			// 
			this.PublicPortTextBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PublicPortTextBox.Location = new System.Drawing.Point(300, 188);
			this.PublicPortTextBox.Name = "PublicPortTextBox";
			this.PublicPortTextBox.Size = new System.Drawing.Size(62, 33);
			this.PublicPortTextBox.TabIndex = 5;
			this.PublicPortTextBox.Text = "80";
			// 
			// PrivateLabel
			// 
			this.PrivateLabel.AutoSize = true;
			this.PrivateLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrivateLabel.Location = new System.Drawing.Point(585, 143);
			this.PrivateLabel.Name = "PrivateLabel";
			this.PrivateLabel.Size = new System.Drawing.Size(84, 32);
			this.PrivateLabel.TabIndex = 14;
			this.PrivateLabel.Text = "Private";
			// 
			// PrivatePortTextBox
			// 
			this.PrivatePortTextBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrivatePortTextBox.Location = new System.Drawing.Point(717, 188);
			this.PrivatePortTextBox.Name = "PrivatePortTextBox";
			this.PrivatePortTextBox.Size = new System.Drawing.Size(62, 33);
			this.PrivatePortTextBox.TabIndex = 18;
			this.PrivatePortTextBox.Text = "64";
			// 
			// PrivateIpLabel
			// 
			this.PrivateIpLabel.AutoSize = true;
			this.PrivateIpLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrivateIpLabel.Location = new System.Drawing.Point(463, 191);
			this.PrivateIpLabel.Name = "PrivateIpLabel";
			this.PrivateIpLabel.Size = new System.Drawing.Size(31, 25);
			this.PrivateIpLabel.TabIndex = 17;
			this.PrivateIpLabel.Text = "IP:";
			// 
			// PrivatePortLabel
			// 
			this.PrivatePortLabel.AutoSize = true;
			this.PrivatePortLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrivatePortLabel.Location = new System.Drawing.Point(662, 191);
			this.PrivatePortLabel.Name = "PrivatePortLabel";
			this.PrivatePortLabel.Size = new System.Drawing.Size(49, 25);
			this.PrivatePortLabel.TabIndex = 16;
			this.PrivatePortLabel.Text = "Port:";
			// 
			// PrivateIpComboBox
			// 
			this.PrivateIpComboBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrivateIpComboBox.FormattingEnabled = true;
			this.PrivateIpComboBox.Location = new System.Drawing.Point(500, 188);
			this.PrivateIpComboBox.Name = "PrivateIpComboBox";
			this.PrivateIpComboBox.Size = new System.Drawing.Size(156, 33);
			this.PrivateIpComboBox.TabIndex = 15;
			// 
			// PublicClientsLabel
			// 
			this.PublicClientsLabel.AutoSize = true;
			this.PublicClientsLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PublicClientsLabel.Location = new System.Drawing.Point(46, 292);
			this.PublicClientsLabel.Name = "PublicClientsLabel";
			this.PublicClientsLabel.Size = new System.Drawing.Size(75, 25);
			this.PublicClientsLabel.TabIndex = 20;
			this.PublicClientsLabel.Text = "client(s)";
			// 
			// PublicSentLabel
			// 
			this.PublicSentLabel.AutoSize = true;
			this.PublicSentLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PublicSentLabel.Location = new System.Drawing.Point(162, 292);
			this.PublicSentLabel.Name = "PublicSentLabel";
			this.PublicSentLabel.Size = new System.Drawing.Size(46, 25);
			this.PublicSentLabel.TabIndex = 22;
			this.PublicSentLabel.Text = "sent";
			// 
			// PublicReceivedLabel
			// 
			this.PublicReceivedLabel.AutoSize = true;
			this.PublicReceivedLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PublicReceivedLabel.Location = new System.Drawing.Point(282, 292);
			this.PublicReceivedLabel.Name = "PublicReceivedLabel";
			this.PublicReceivedLabel.Size = new System.Drawing.Size(81, 25);
			this.PublicReceivedLabel.TabIndex = 24;
			this.PublicReceivedLabel.Text = "received";
			// 
			// PrivateReceivedLabel
			// 
			this.PrivateReceivedLabel.AutoSize = true;
			this.PrivateReceivedLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrivateReceivedLabel.Location = new System.Drawing.Point(699, 292);
			this.PrivateReceivedLabel.Name = "PrivateReceivedLabel";
			this.PrivateReceivedLabel.Size = new System.Drawing.Size(81, 25);
			this.PrivateReceivedLabel.TabIndex = 30;
			this.PrivateReceivedLabel.Text = "received";
			// 
			// PrivateSentLabel
			// 
			this.PrivateSentLabel.AutoSize = true;
			this.PrivateSentLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrivateSentLabel.Location = new System.Drawing.Point(579, 292);
			this.PrivateSentLabel.Name = "PrivateSentLabel";
			this.PrivateSentLabel.Size = new System.Drawing.Size(46, 25);
			this.PrivateSentLabel.TabIndex = 28;
			this.PrivateSentLabel.Text = "sent";
			// 
			// PrivateClientsLabel
			// 
			this.PrivateClientsLabel.AutoSize = true;
			this.PrivateClientsLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PrivateClientsLabel.Location = new System.Drawing.Point(463, 292);
			this.PrivateClientsLabel.Name = "PrivateClientsLabel";
			this.PrivateClientsLabel.Size = new System.Drawing.Size(75, 25);
			this.PrivateClientsLabel.TabIndex = 26;
			this.PrivateClientsLabel.Text = "client(s)";
			// 
			// LinksLabel
			// 
			this.LinksLabel.AutoSize = true;
			this.LinksLabel.Font = new System.Drawing.Font("Segoe UI Semilight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LinksLabel.Location = new System.Drawing.Point(384, 157);
			this.LinksLabel.Name = "LinksLabel";
			this.LinksLabel.Size = new System.Drawing.Size(59, 25);
			this.LinksLabel.TabIndex = 32;
			this.LinksLabel.Text = "link(s)";
			// 
			// LinksPictureBox
			// 
			this.LinksPictureBox.Image = global::wb_Gateway.Properties.Resources.link_48;
			this.LinksPictureBox.InitialImage = null;
			this.LinksPictureBox.Location = new System.Drawing.Point(389, 106);
			this.LinksPictureBox.Name = "LinksPictureBox";
			this.LinksPictureBox.Size = new System.Drawing.Size(48, 48);
			this.LinksPictureBox.TabIndex = 31;
			this.LinksPictureBox.TabStop = false;
			// 
			// PrivateReceivedPictureBox
			// 
			this.PrivateReceivedPictureBox.Image = global::wb_Gateway.Properties.Resources.gps_receiving_48;
			this.PrivateReceivedPictureBox.InitialImage = null;
			this.PrivateReceivedPictureBox.Location = new System.Drawing.Point(704, 241);
			this.PrivateReceivedPictureBox.Name = "PrivateReceivedPictureBox";
			this.PrivateReceivedPictureBox.Size = new System.Drawing.Size(48, 48);
			this.PrivateReceivedPictureBox.TabIndex = 29;
			this.PrivateReceivedPictureBox.TabStop = false;
			// 
			// PrivateSentPictureBox
			// 
			this.PrivateSentPictureBox.Image = global::wb_Gateway.Properties.Resources.satellite2_sending_signal_48;
			this.PrivateSentPictureBox.InitialImage = null;
			this.PrivateSentPictureBox.Location = new System.Drawing.Point(584, 241);
			this.PrivateSentPictureBox.Name = "PrivateSentPictureBox";
			this.PrivateSentPictureBox.Size = new System.Drawing.Size(48, 48);
			this.PrivateSentPictureBox.TabIndex = 27;
			this.PrivateSentPictureBox.TabStop = false;
			// 
			// PrivateClientsPictureBox
			// 
			this.PrivateClientsPictureBox.Image = global::wb_Gateway.Properties.Resources.group_48;
			this.PrivateClientsPictureBox.InitialImage = null;
			this.PrivateClientsPictureBox.Location = new System.Drawing.Point(468, 241);
			this.PrivateClientsPictureBox.Name = "PrivateClientsPictureBox";
			this.PrivateClientsPictureBox.Size = new System.Drawing.Size(48, 48);
			this.PrivateClientsPictureBox.TabIndex = 25;
			this.PrivateClientsPictureBox.TabStop = false;
			// 
			// PublicReceivedPictureBox
			// 
			this.PublicReceivedPictureBox.Image = global::wb_Gateway.Properties.Resources.gps_receiving_48;
			this.PublicReceivedPictureBox.InitialImage = null;
			this.PublicReceivedPictureBox.Location = new System.Drawing.Point(287, 241);
			this.PublicReceivedPictureBox.Name = "PublicReceivedPictureBox";
			this.PublicReceivedPictureBox.Size = new System.Drawing.Size(48, 48);
			this.PublicReceivedPictureBox.TabIndex = 23;
			this.PublicReceivedPictureBox.TabStop = false;
			// 
			// PublicSentPictureBox
			// 
			this.PublicSentPictureBox.Image = global::wb_Gateway.Properties.Resources.satellite2_sending_signal_48;
			this.PublicSentPictureBox.InitialImage = null;
			this.PublicSentPictureBox.Location = new System.Drawing.Point(167, 241);
			this.PublicSentPictureBox.Name = "PublicSentPictureBox";
			this.PublicSentPictureBox.Size = new System.Drawing.Size(48, 48);
			this.PublicSentPictureBox.TabIndex = 21;
			this.PublicSentPictureBox.TabStop = false;
			// 
			// PublicClientsPictureBox
			// 
			this.PublicClientsPictureBox.Image = global::wb_Gateway.Properties.Resources.group_48;
			this.PublicClientsPictureBox.InitialImage = null;
			this.PublicClientsPictureBox.Location = new System.Drawing.Point(51, 241);
			this.PublicClientsPictureBox.Name = "PublicClientsPictureBox";
			this.PublicClientsPictureBox.Size = new System.Drawing.Size(48, 48);
			this.PublicClientsPictureBox.TabIndex = 19;
			this.PublicClientsPictureBox.TabStop = false;
			// 
			// PublicPictureBox
			// 
			this.PublicPictureBox.Image = global::wb_Gateway.Properties.Resources.public_128;
			this.PublicPictureBox.InitialImage = null;
			this.PublicPictureBox.Location = new System.Drawing.Point(133, 12);
			this.PublicPictureBox.Name = "PublicPictureBox";
			this.PublicPictureBox.Size = new System.Drawing.Size(128, 128);
			this.PublicPictureBox.TabIndex = 13;
			this.PublicPictureBox.TabStop = false;
			// 
			// PrivatePictureBox
			// 
			this.PrivatePictureBox.Image = global::wb_Gateway.Properties.Resources.private_128;
			this.PrivatePictureBox.InitialImage = null;
			this.PrivatePictureBox.Location = new System.Drawing.Point(562, 12);
			this.PrivatePictureBox.Name = "PrivatePictureBox";
			this.PrivatePictureBox.Size = new System.Drawing.Size(128, 128);
			this.PrivatePictureBox.TabIndex = 12;
			this.PrivatePictureBox.TabStop = false;
			// 
			// StartButton
			// 
			this.StartButton.BackgroundImage = global::wb_Gateway.Properties.Resources.vpn_32;
			this.StartButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.StartButton.Font = new System.Drawing.Font("Segoe UI Semilight", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StartButton.Location = new System.Drawing.Point(368, 12);
			this.StartButton.Name = "StartButton";
			this.StartButton.Size = new System.Drawing.Size(89, 88);
			this.StartButton.TabIndex = 11;
			this.StartButton.Text = "Start";
			this.StartButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.StartButton.UseVisualStyleBackColor = true;
			this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// WinTimer
			// 
			this.WinTimer.Enabled = true;
			this.WinTimer.Tick += new System.EventHandler(this.WinTimer_Tick);
			// 
			// MainWin
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(830, 332);
			this.Controls.Add(this.LinksLabel);
			this.Controls.Add(this.LinksPictureBox);
			this.Controls.Add(this.PrivateReceivedLabel);
			this.Controls.Add(this.PrivateReceivedPictureBox);
			this.Controls.Add(this.PrivateSentLabel);
			this.Controls.Add(this.PrivateSentPictureBox);
			this.Controls.Add(this.PrivateClientsLabel);
			this.Controls.Add(this.PrivateClientsPictureBox);
			this.Controls.Add(this.PublicReceivedLabel);
			this.Controls.Add(this.PublicReceivedPictureBox);
			this.Controls.Add(this.PublicSentLabel);
			this.Controls.Add(this.PublicSentPictureBox);
			this.Controls.Add(this.PublicClientsLabel);
			this.Controls.Add(this.PublicClientsPictureBox);
			this.Controls.Add(this.PrivatePortTextBox);
			this.Controls.Add(this.PrivateIpLabel);
			this.Controls.Add(this.PrivatePortLabel);
			this.Controls.Add(this.PrivateIpComboBox);
			this.Controls.Add(this.PrivateLabel);
			this.Controls.Add(this.PublicPictureBox);
			this.Controls.Add(this.PrivatePictureBox);
			this.Controls.Add(this.StartButton);
			this.Controls.Add(this.PublicPortTextBox);
			this.Controls.Add(this.PublicIpLabel);
			this.Controls.Add(this.PublicPortLabel);
			this.Controls.Add(this.PublicIpComboBox);
			this.Controls.Add(this.PublicLabel);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainWin";
			this.Text = "Web Gateway";
			((System.ComponentModel.ISupportInitialize)(this.LinksPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PrivateReceivedPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PrivateSentPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PrivateClientsPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PublicReceivedPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PublicSentPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PublicClientsPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PublicPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PrivatePictureBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label PublicLabel;
		private System.Windows.Forms.ComboBox PublicIpComboBox;
		private System.Windows.Forms.Label PublicPortLabel;
		private System.Windows.Forms.Label PublicIpLabel;
		private System.Windows.Forms.TextBox PublicPortTextBox;
		private System.Windows.Forms.Button StartButton;
		private System.Windows.Forms.PictureBox PrivatePictureBox;
		private System.Windows.Forms.PictureBox PublicPictureBox;
		private System.Windows.Forms.Label PrivateLabel;
		private System.Windows.Forms.TextBox PrivatePortTextBox;
		private System.Windows.Forms.Label PrivateIpLabel;
		private System.Windows.Forms.Label PrivatePortLabel;
		private System.Windows.Forms.ComboBox PrivateIpComboBox;
		private System.Windows.Forms.PictureBox PublicClientsPictureBox;
		private System.Windows.Forms.Label PublicClientsLabel;
		private System.Windows.Forms.PictureBox PublicSentPictureBox;
		private System.Windows.Forms.Label PublicSentLabel;
		private System.Windows.Forms.Label PublicReceivedLabel;
		private System.Windows.Forms.PictureBox PublicReceivedPictureBox;
		private System.Windows.Forms.Label PrivateReceivedLabel;
		private System.Windows.Forms.PictureBox PrivateReceivedPictureBox;
		private System.Windows.Forms.Label PrivateSentLabel;
		private System.Windows.Forms.PictureBox PrivateSentPictureBox;
		private System.Windows.Forms.Label PrivateClientsLabel;
		private System.Windows.Forms.PictureBox PrivateClientsPictureBox;
		private System.Windows.Forms.Label LinksLabel;
		private System.Windows.Forms.PictureBox LinksPictureBox;
		private System.Windows.Forms.Timer WinTimer;
	}
}

