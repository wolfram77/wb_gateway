﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Wasp.Net;
using Wasp.Logging;

namespace wb_Gateway
{
	public partial class MainWin : Form
	{
		GtwyHost Gateway;

		public MainWin()
		{
			InitializeComponent();
			IPAddress[] ipAddrs = NetHost.Ipv4Addr;
			PublicIpComboBox.Items.AddRange(ipAddrs);
			PrivateIpComboBox.Items.AddRange(ipAddrs);
			PublicIpComboBox.Text = ipAddrs[0].ToString();
			PrivateIpComboBox.Text = ipAddrs[0].ToString();
		}

		private void StartButton_Click(object sender, EventArgs e)
		{
			switch ( StartButton.Text )
			{
				case "Start":
					try
					{
						IPEndPoint publicAddr = new IPEndPoint(IPAddress.Parse(PublicIpComboBox.Text), int.Parse(PublicPortTextBox.Text));
						IPEndPoint privateAddr = new IPEndPoint(IPAddress.Parse(PrivateIpComboBox.Text), int.Parse(PrivatePortTextBox.Text));
						PublicIpComboBox.Enabled = false;
						PublicPortTextBox.Enabled = false;
						PrivateIpComboBox.Enabled = false;
						PrivatePortTextBox.Enabled = false;
						Gateway = new GtwyHost(publicAddr, privateAddr);
						StartButton.Text = "Stop";
					}
					catch ( Exception ) { }
					break;
				case "Stop":
					Gateway.Close();
					Gateway = null;
					PublicIpComboBox.Enabled = true;
					PublicPortTextBox.Enabled = true;
					PrivateIpComboBox.Enabled = true;
					PrivatePortTextBox.Enabled = true;
					StartButton.Text = "Start";
					break;
			}
		}
		private void WinTimer_Tick(object sender, EventArgs e)
		{
			int links = 0;
			int publicClients = 0, publicSent = 0, publicReceived = 0;
			int privateClients = 0, privateSent = 0, privateReceived = 0;
			if ( Gateway != null )
			{
				links = Gateway.Conns;
				/*
				publicClients = Gateway.PublicClients;
				publicSent = Gateway.extTx;
				publicReceived = Gateway.extRx;
				privateClients = Gateway.LocalClients;
				privateSent = Gateway.localTx;
				privateReceived = Gateway.localRx;
				*/
			}
			LinksLabel.Text = links + " link(s)";
			PublicClientsLabel.Text = publicClients + " client(s)";
			PublicSentLabel.Text = publicSent + " sent";
			PublicReceivedLabel.Text = publicReceived + " received";
			PrivateClientsLabel.Text = privateClients + " client(s)";
			PrivateSentLabel.Text = privateSent + " sent";
			PrivateReceivedLabel.Text = privateReceived + " received";
		}
	}
}
