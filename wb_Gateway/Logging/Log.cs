﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace Wasp.Logging
{
	static class Log
	{
		public static string LogPath = @"Log";
		static FileStream cmnFile;
		static ConcurrentDictionary<int, FileStream> threadFile = new ConcurrentDictionary<int, FileStream>();
		static ConcurrentDictionary<string, FileStream> classFile = new ConcurrentDictionary<string, FileStream>();

		static void writeLogs(string className, string format, params object[] args)
		{
			FileStream thdFile = null, clsFile = null;
			// open common log
			if ( cmnFile == null )
			{
				Directory.CreateDirectory(LogPath);
				string fileName = Path.Combine(LogPath, "Log-Common.txt");
				cmnFile = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
			}
			// open thread-specific log
			int threadId = Thread.CurrentThread.ManagedThreadId;
			threadFile.TryGetValue(threadId, out thdFile);
			if ( thdFile == null )
			{
				string fileName = Path.Combine(LogPath, string.Format("Log-Thread[{0}].txt", threadId));
				thdFile = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
				threadFile[threadId] = thdFile;
			}
			// open class-specific log
			classFile.TryGetValue(className, out clsFile);
			if ( clsFile == null )
			{
				string fileName = Path.Combine(LogPath, string.Format("Log-Class[{0}].txt", className));
				clsFile = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
				classFile[className] = clsFile;
			}
			// convert args to string
			for ( int i=0; i < args.Length; i++ )
			{
				try { args[i] = args[i].ToString(); }
				catch ( Exception ) { args[i] = "null"; }
			}
			// get log data
			format = string.Format("[class:{0}, thread:{1}, time:{2}] - ({3})", className, threadId, DateTime.Now, format);
			byte[] data = Encoding.ASCII.GetBytes(string.Format(format, args));
			// write to log files
			cmnFile.Write(data, 0, data.Length);
			thdFile.Write(data, 0, data.Length);
			clsFile.Write(data, 0, data.Length);
			cmnFile.Flush(true);
			thdFile.Flush(true);
			clsFile.Flush(true);
		}
		public static void Write(string className, string format, params object[] args)
		{
			writeLogs(className, format, args);
		}
		public static void Write(string className, object arg)
		{
			writeLogs(className, "{0}", arg);
		}
		public static void WriteLine(string className, string format, params object[] args)
		{
			writeLogs(className, format + "\r\n", args);
		}
		public static void WriteLine(string className, object arg)
		{
			writeLogs(className, "{0}\r\n", arg);
		}
	}
}
