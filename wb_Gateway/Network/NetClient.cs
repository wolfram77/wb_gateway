﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Wasp.Net
{
	// Network Client
	public class NetClient
	{
		Socket sckt;	// link to the client
		object info;	// attachable info
		DataBlk buff;	// internal packet buffer
		int noRxCount;	// times, nothing was received
		bool useFormat;	// use size field format or not
		int txPkts, rxPkts;	// sent, received packets
		long txBytes, rxBytes;	// sent, received bytes of data
		public NetEventFnGroup eventFn;	// event functions
		BlockingCollection<NetPkt> txPkt;	// list of requests made (connect / diconnect / send)
		BlockingCollection<NetPkt> rxPkt;	// list of packets received (normally filled up by NetHost)
		public object Info
		{
			get { return info; }
			set { info = value; }
		}
		public int TxPkts
		{
			get { return txPkts; }
		}
		public int RxPkts
		{
			get { return rxPkts; }
		}
		public long TxBytes
		{
			get { return txBytes; }
		}
		public long RxBytes
		{
			get { return rxBytes; }
		}
		public bool UseFormat
		{
			get { return useFormat; }
			set { useFormat = true; }
		}
		public NetConn Conn
		{
			get
			{ return (sckt != null) ? new NetConn(sckt.LocalEndPoint, sckt.RemoteEndPoint) : default(NetConn); }
		}	// client connection address (local, remote)
		public bool Linked
		{
			get
			{ return (sckt != null) && sckt.Connected; }
		}	// connection check
		public bool LinkOk
		{
			get
			{
				if ( !Linked ) return false;
				try { sckt.Send(new byte[0]); }
				catch ( Exception ) { }
				return sckt.Connected;
			}
		}	// full connection check
		public NetEventFnGroup EventFn
		{
			get { return eventFn; }
			set { eventFn = value; }
		}
		public BlockingCollection<NetPkt> RxPkt
		{
			set { rxPkt = value; }
		}
		const int noRxLimit = 4;		// no data receive limit
		const int buffSize = 4096;		// size of internal buffer

		public NetClient(Socket sckt, NetEventFnGroup eventFn, BlockingCollection<NetPkt> rxPkt, bool useFormat = false, object info = null)
		{
			this.info = info;
			this.useFormat = useFormat;
			txPkt = new BlockingCollection<NetPkt>();
			if ( useFormat ) buff = new DataBlk(new byte[buffSize]);
			this.eventFn = (eventFn != null) ? eventFn : new NetEventFnGroup();
			this.rxPkt = (rxPkt != null) ? rxPkt : new BlockingCollection<NetPkt>();
			if ( sckt != null )
			{
				this.sckt = sckt;
				// start the send and receive workers
				ThreadPool.QueueUserWorkItem(new WaitCallback(worker_Tx));
				ThreadPool.QueueUserWorkItem(new WaitCallback(worker_Rx));
			}
		}

		void tx_Any()
		{
			// take a request, send
			Exception err = null;
			NetPkt pkt = txPkt.Take();
			if ( pkt.Data.Src == null ) throw new InvalidOperationException();
			try
			{
				sckt.Send(pkt.Data.Src, pkt.Data.Off, pkt.Data.Size, SocketFlags.None);
				txBytes += pkt.Data.Size;
				txPkts++;
			}
			catch ( Exception e ) { err = e; }
			try { eventFn.OnTx(this, new NetEventArgs(NetEvent.Tx, pkt, err)); }
			catch ( Exception ) { }
		}
		void rx_NoFormat()
		{
			int avail = sckt.Available;
			// if no data too many times, terminate
			if ( noRxCount >= noRxLimit ) throw new InvalidOperationException();
			if ( avail == 0 ) { noRxCount++; return; }
			// receive data
			buff = new DataBlk(new byte[avail]);
			sckt.Receive(buff.Src);
			NetPkt pkt = new NetPkt(buff, Conn);
			if ( rxPkt != null ) rxPkt.Add(pkt);
			// update received
			rxPkts++;
			rxBytes += avail;
			// call event handler
			try { eventFn.OnRx(this, new NetEventArgs(NetEvent.Rx, pkt)); }
			catch ( Exception ) { }
		}
		void rx_WithFormat()
		{
			// low data? get some to buffer
			if ( buff.Off < 2 || buff.Off < buff.Size )
			{
				int avail = sckt.Available;
				sckt.Receive(buff.Src, buff.Off, avail, SocketFlags.None);
				buff.Off += avail;
				// if no data too many times, terminate
				if ( avail == 0 ) noRxCount++;
				if ( noRxCount > noRxLimit ) throw new InvalidOperationException();
			}
			// get packet size
			if ( buff.Off >= 2 )
			{
				buff.Size = BitConverter.ToUInt16(buff.Src, 0);
				if ( buff.Size < 2 ) buff.Size = 2;
			}
			// dont go deeper without full packet
			if ( buff.Off < 2 || buff.Off < buff.Size ) return;
			// get packet from the buffer
			byte[] data = new byte[buff.Size];
			Array.Copy(buff.Src, data, buff.Size);
			NetPkt pkt = new NetPkt(new DataBlk(data), Conn);
			if ( rxPkt != null ) rxPkt.Add(pkt);
			// update received
			rxPkts++;
			rxBytes += buff.Size;
			// clean buffer for next packet
			Array.Copy(buff.Src, buff.Size, buff.Src, 0, buff.Off - buff.Size);
			buff.Off -= buff.Size;
			buff.Size = 0;
			// call event handler
			try { eventFn.OnRx(this, new NetEventArgs(NetEvent.Rx, pkt)); }
			catch ( Exception ) { }
		}
		void worker_Tx(object d)
		{
			try
			{
				while ( sckt != null )
				{ tx_Any(); }
			}
			catch ( Exception ) { }
			Break();
		}
		void worker_Rx(object d)
		{
			try
			{
				while ( sckt != null )
				{
					if ( UseFormat ) rx_WithFormat();
					else rx_NoFormat();
				}
			}
			catch ( Exception ) { }
			Break();
		}
		void LinkSync(object d)
		{
			Exception err = null;
			// close link, if still open
			if ( sckt != null ) BreakSync(null);
			try
			{
				// connect to client
				IPEndPoint rmtAddr = (IPEndPoint) d;
				sckt = new Socket(rmtAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				sckt.Connect(rmtAddr);
				// check connection success, if not
				if ( !LinkOk )
				{
					// close the link
					sckt.Close();
					sckt = null;
					throw new SocketException((int) SocketError.ConnectionReset);
				}
				// on success, start the send and receive workers
				ThreadPool.QueueUserWorkItem(new WaitCallback(worker_Tx));
				ThreadPool.QueueUserWorkItem(new WaitCallback(worker_Rx));
			}
			catch ( Exception e ) { err = e; }
			try { eventFn.OnLink(this, new NetEventArgs(NetEvent.Link, d, err)); }
			catch ( Exception ) { }
		}
		void BreakSync(object d)
		{
			// stop the send handler
			try { txPkt.Add(new NetPkt()); }
			catch ( Exception ) { }
			// call event handler
			try { eventFn.OnBreak(this, new NetEventArgs(NetEvent.Break)); }
			catch ( Exception ) { }
			// break link (stops receive handler)
			try
			{
				if ( sckt.Connected )
				{
					sckt.Shutdown(SocketShutdown.Both);
					sckt.Disconnect(false);
				}
			}
			catch ( Exception ) { }
			try { sckt.Close(); sckt = null; }
			catch ( Exception ) { }
			// clean up
			buff.Src = null;
			buff.Size = 0;
			eventFn = null;
			if ( txPkt != null )
			{
				txPkt.Dispose();
				txPkt = null;
			}
			rxPkt = null;
		}

		public NetPkt Rx(CancellationToken cancelRx)
		{
			return rxPkt.Take(cancelRx);
		}
		public NetPkt Rx()
		{
			return rxPkt.Take();
		}
		public void Tx(NetPkt pkt)
		{
			txPkt.Add(pkt);
		}
		public void Tx(DataBlk data)
		{
			Tx(new NetPkt(data));
		}
		public void OnAdd()
		{
			try { eventFn.OnAdd(this, new NetEventArgs(NetEvent.Add)); }
			catch ( Exception ) { }
		}
		public void Link(IPEndPoint addr = null)
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(LinkSync), addr);
		}
		public void Break()
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(BreakSync));
		}
		public new string ToString()
		{
			return Conn.ToString(); 
		}
	}
}
