﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Wasp.Net
{
	// Network Host (Server)
	public class NetHost
	{
		Socket sckt;	// host socket
		object info;	// attachable info
		bool useFormat;	// use size field format or not
		NetEventFnGroup eventFn;	/// event functions
		BlockingCollection<NetPkt> rxPkt;	// list of packets received
		Dictionary<NetConn, NetClient> client;	// list of clients
		public NetClient this[NetConn client]
		{
			get
			{ return this.client[client]; }
		}		// client with a given address
		public int TxPkts
		{
			get
			{
				int txPkts = 0;
				foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
					txPkts += clntKV.Value.TxPkts;
				return txPkts;
			}
		}
		public int RxPkts
		{
			get
			{
				int rxPkts = 0;
				foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
					rxPkts += clntKV.Value.RxPkts;
				return rxPkts;
			}
		}
		public long TxBytes
		{
			get
			{
				long txBytes = 0;
				foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
					txBytes += clntKV.Value.TxBytes;
				return txBytes;
			}
		}
		public long RxBytes
		{
			get
			{
				long rxBytes = 0;
				foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
					rxBytes += clntKV.Value.RxBytes;
				return rxBytes;
			}
		}
		public object Info
		{
			get { return info; }
			set { info = value; }
		}
		public int Clients
		{
			get { return client.Count; }
		}
		public bool UseFormat
		{
			get { return useFormat; }
			set { useFormat = value; }
		}
		public NetConn Conn
		{
			get { return new NetConn(sckt.LocalEndPoint, sckt.RemoteEndPoint); }
		}		// host connection (endpoint)
		public static string Name
		{
			get
			{ return Dns.GetHostName(); }
		}		// host name
		public NetEventFnGroup EventFn
		{
			get { return eventFn; }
			set { eventFn = value; }
		}
		public static IPAddress[] IpAddr
		{
			get
			{ return Dns.GetHostAddresses(Name); }
		}		// available IP addresses
		public static IPAddress[] Ipv4Addr
		{
			get
			{ return (IPAddress[]) IpAddr.Where(ip => (ip.AddressFamily == AddressFamily.InterNetwork)).ToArray(); }
		}	// available IPv4 addresses
		public static IPAddress[] Ipv6Addr
		{
			get
			{ return (IPAddress[]) IpAddr.Where(ip => (ip.AddressFamily == AddressFamily.InterNetworkV6)).ToArray(); }
		}	// available IPv6 addresses
		const int clientsChkTime = 5000;	// time gap between client checks (in ms)
		const int clientChkTime = 100;		// time gap between each client check (in ms)
		const int maxPendingConns = 64;		// max pending connect requests

		public NetHost(IPEndPoint addr, NetEventFnGroup eventFn, bool useFormat = false, object info = null)
		{
			// initialize
			this.info = info;
			this.useFormat = useFormat;
			this.eventFn = (eventFn != null) ? eventFn : new NetEventFnGroup();
			rxPkt = new BlockingCollection<NetPkt>();
			client = new Dictionary<NetConn, NetClient>();
			// create host (server)
			sckt = new Socket(addr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			sckt.Bind(addr);
			sckt.Listen(maxPendingConns);
			// start the workers
			ThreadPool.QueueUserWorkItem(new WaitCallback(worker_Add));
			ThreadPool.QueueUserWorkItem(new WaitCallback(worker_Break));
		}

		void worker_Add(object d)
		{
			try
			{
				// terminate if inactive
				while ( sckt != null )
				{
					// accept connection from client
					Socket clntSckt = sckt.Accept();
					// add it to client list
					NetClient netClnt = new NetClient(clntSckt, eventFn, rxPkt, useFormat, info);
					client[netClnt.Conn] = netClnt;
					netClnt.OnAdd();
				}
			}
			catch ( Exception ) { }
		}
		void worker_Break(object d)
		{
			try
			{
				// terminate if inactive
				while ( sckt != null )
				{
					// sleep for some time
					Thread.Sleep(clientsChkTime);
					foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
					{
						Thread.Sleep(clientChkTime);
						NetClient clnt = clntKV.Value;
						bool linked = clnt.LinkOk;
						if ( !linked )
						{
							clnt.Break();
							client.Remove(clntKV.Key);
						}
					}
				}
			}
			catch ( Exception ) { }
		}
		void tmp_OnLink(object sender, NetEventArgs args)
		{
			// add to client list
			NetClient clnt = (NetClient) sender;
			client[clnt.Conn] = clnt;
			// update event handlers
			clnt.EventFn = eventFn;
			// call actual event handler
			try { eventFn.OnLink(sender, args); }
			catch ( Exception ) { }
		}

		public NetPkt Rx()
		{
			return rxPkt.Take();
		}
		public NetPkt Rx(CancellationToken cancelRx)
		{
			return rxPkt.Take(cancelRx);
		}
		public void Tx(NetConn conn, DataBlk data)
		{
			if ( conn.Equals(default(NetConn)) ) Tx(data);
			else client[conn].Tx(data);
		}
		public void Tx(DataBlk data)
		{
			foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
				clntKV.Value.Tx(data);
		}
		public void Add(NetClient clnt)
		{
			client[clnt.Conn] = clnt;
			// clnt.OnAdd();
		}
		public void Link(IPEndPoint addr)
		{
			NetEventFnGroup tmpEventFn = new NetEventFnGroup(null, null, null, tmp_OnLink, null);
			NetClient clnt = new NetClient(null, tmpEventFn, rxPkt, useFormat, info);
			clnt.Link(addr);
		}
		public void Break(NetConn conn = default(NetConn))
		{
			if ( !conn.Equals(default(NetConn)) )
			{
				client[conn].Break();
				client.Remove(conn);
			}
			else
			{
				foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
					clntKV.Value.Break();
				client.Clear();
			}
		}
		public bool Linked(NetConn conn = default(NetConn))
		{
			if ( !conn.Equals(default(NetConn)) ) return client.ContainsKey(conn);
			bool lnkd = true;
			foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
				lnkd &= clntKV.Value.Linked;
			return lnkd;
		}
		public bool LinkOk(NetConn conn = default(NetConn))
		{
			if ( !conn.Equals(default(NetConn)) ) return client.ContainsKey(conn) && client[conn].LinkOk;
			bool lnkOk = true;
			foreach ( KeyValuePair<NetConn, NetClient> clntKV in client )
				lnkOk &= clntKV.Value.LinkOk;
			return lnkOk;
		}
		public void Close()
		{
			// end accept handler
			sckt.Close();
			sckt = null;
			// close all
			Break();
			// clean up
			client.Clear();
			client = null;
			rxPkt.Dispose();
			rxPkt = null;
		}
		public new string ToString()
		{
			return Conn.ToString();
		}
	}
}
