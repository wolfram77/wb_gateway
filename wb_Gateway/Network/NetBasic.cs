﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Wasp.Type;

namespace Wasp.Net
{
	// network event / request
	public enum NetEvent
	{
		Rx,		// receive ok
		Tx,		// send ok / failed
		Add,	// new client
		Link,	// connect ok / failed
		Break,	// disconnect ok
	}

	// network event arguments / requests
	public class NetEventArgs : EventArgs
	{
		public NetEvent Event;		// event / request
		public object Data;			// rvent / request data
		public Exception Error;		// null if ok

		public NetEventArgs()
			: base()
		{
		}
		public NetEventArgs(NetEvent evnt)
			: base()
		{
			Event = evnt;
		}
		public NetEventArgs(NetEvent evnt, object data)
			: base()
		{
			Event = evnt;
			Data = data;
		}
		public NetEventArgs(NetEvent evnt, object data, Exception error)
			: base()
		{
			Event = evnt;
			Data = data;
			Error = error;
		}
	}

	// network event function form
	public delegate void NetEventFn(object sender, NetEventArgs args);

	// network event function group
	public class NetEventFnGroup
	{
		public NetEventFn OnRx, OnTx;	// receive, send handlers
		public NetEventFn OnAdd, OnLink, OnBreak;	// new client, connect, disconnect handlers

		public NetEventFnGroup() { }
		public NetEventFnGroup(NetEventFn onRx, NetEventFn onTx, NetEventFn onAdd, NetEventFn onLink, NetEventFn onBreak)
		{
			OnRx = onRx;
			OnTx = onTx;
			OnAdd = onAdd;
			OnLink = onLink;
			OnBreak = onBreak;
		}
	}

	// data block
	public struct DataBlk
	{
		public byte[] Src;
		public int Off;
		public int Size;

		public DataBlk(byte[] src)
		{
			Src = src;
			Off = 0;
			Size = (src != null)? src.Length : 0;
		}
		public DataBlk(byte[] src, int off)
		{
			Src = src;
			Off = off;
			Size = (src != null) ? src.Length - off : 0;
		}
		public DataBlk(byte[] src, int off, int size)
		{
			Src = src;
			Off = off;
			Size = size;
		}
	}

	// network connection
	public struct NetConn
	{
		public IPEndPoint Local;
		public IPEndPoint Remote;

		public NetConn(EndPoint local, EndPoint remote)
		{
			Local = (IPEndPoint) local;
			Remote = (IPEndPoint) remote;
		}
		public NetConn(byte[] src, int off)
		{
			Local = TypeConverter.ReadIpEndPoint(src, off);
			off += 6;
			Remote = TypeConverter.ReadIpEndPoint(src, off);
			off += 6;
		}

		public void Write(byte[] dst, int off)
		{
			TypeConverter.Write(dst, off, Local);
			off += 6;
			TypeConverter.Write(dst, off, Remote);
			off += 6;
		}
		public new string ToString()
		{
			return string.Format("[local-{0}, remote-{1}]", Local.ToString(), Remote.ToString());
		}
	}

	// network packet
	// <size: uint16> <data: byte[size]>
	public struct NetPkt
	{
		public DataBlk Data;	// packet data
		public NetConn Conn;	// destination address

		public NetPkt(DataBlk data)
		{
			Data = data;
			Conn = default(NetConn);
		}
		public NetPkt(DataBlk data, NetConn conn)
		{
			Data = data;
			Conn = conn;
		}
	}

	// network checksum
	public class NetChecksum
	{
		// Need Hash 8
		public static ushort Xor16(DataBlk data)
		{
			// Find 16-bit XORed checksum
			ushort chksum = 0;
			int limit = data.Size & (~1);
			for ( int i = 0; i < limit; i += 2 )
				chksum ^= BitConverter.ToUInt16(data.Src, data.Off + i);
			if ( limit < data.Size ) chksum ^= data.Src[data.Off + limit];
			return chksum;
		}
	}
}
