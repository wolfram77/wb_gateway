﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Wasp.Type
{
	public static class TypeConverter
	{
		public static IPAddress ReadIpAddr(byte[] src, int off)
		{
			byte[] ipBytes = new byte[4];
			Array.Copy(src, off, ipBytes, 0, 4);
			return new IPAddress(ipBytes);
		}
		public static IPEndPoint ReadIpEndPoint(byte[] src, int off)
		{
			IPAddress ipAddr = ReadIpAddr(src, off);
			int port = BitConverter.ToUInt16(src, off + 4);
			return new IPEndPoint(ipAddr, port);
		}
		public static void Write(byte[] dst, int off, IPAddress value)
		{
			value = (value != null) ? value : new IPAddress(0);
			Array.Copy(value.GetAddressBytes(), 0, dst, off, 4);
		}
		public static void Write(byte[] dst, int off, IPEndPoint value)
		{
			value = (value != null) ? value : new IPEndPoint(0, 0);
			Write(dst, off, value.Address);
			Array.Copy(BitConverter.GetBytes(value.Port), 0, dst, off + 4, 2);
		}
	}
}
